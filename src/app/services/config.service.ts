import { Injectable } from '@angular/core';
import { singleSpaPropsSubject } from 'src/single-spa/single-spa-props';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  baseUrl: string;
  locale: string;
  siteCode: string;
  environment: string;
  resellerId: string;
  load() {
    return new Promise(resolve => {
      singleSpaPropsSubject.subscribe(props => {
        this.baseUrl = props.baseUrl;
        this.locale = props.locale;
        this.siteCode = props.siteCode;
        this.environment = props.environment;
        this.resellerId = props.resellerId;
        resolve(true);
      }, err => console.log(err));
    });
}
}
