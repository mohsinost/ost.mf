import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { startWith, scan, map, publish, refCount } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoadingAnimationService {
  busyStateObservable: Observable<boolean>;
  private busySubject: Subject<number>;
  constructor() { 
    this.busySubject = new Subject<number>();

    this.busyStateObservable = this.busySubject.pipe(
      startWith(0),
      scan((total, i) => Math.max(0, total + i), 0),
      map(v => v > 0),
      publish(),
      refCount());
  }
  push() {
    this.busySubject.next(1);
  }

  pop() {
    this.busySubject.next(-1);
  }
}
