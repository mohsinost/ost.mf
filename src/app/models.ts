export class OrderSearchRequest {
    resellerPoNumber: string;
    ciscoOrderNumber: string;
    endUserPONumber: string;
    imOrderNumber: string;
    imSKU: string;
    imVPN: string;
    imSerialNos: string;
    imVendor: string;
    imBidNo: string;
    dateFilter: number; // 1 = Last 30 days, 2 = All
    fromDate: Date;
    toDate: Date;
    shipDateFrom: Date;
    shipDateTo: Date;
    status: string; // O = Open, C = Closed
    orderStatus: string; // Use OrderStatus when Orderstatus is explicitly known
    sortColumn: string;
    sortDirection: string;
    recordsPerPage: number;
    requestedPageNumber: number;
    useProd: boolean;
    bypassWhiteList: boolean;
    incBranch: string;
    invoiceNumber: string;
    invoiceDateFrom: Date;
    invoiceDateTo: Date;
    voidedStatus: boolean;
    displayFilteredOrders: boolean;
  }
  
  export class OrderSearchResponse {
    errorMessage: string;
    totalNumResults: number;
    orderSummaries: Order[];
  }
  
  export class Order {
    customerPONumber: string;
    imOrderNumber: string;
    invoiceNumber: string;
    soNumber: string;
    orderDate: string;
    orderDateWithTime: string;
    orderStatus: string;
    statusCode: string;
    orderTotal: string;
    orderTotalAmount: number;
    endUser: string;
    customerNumber: string;
    endUserPONumber: string;
    orderType: string;
    shipDate: string;
    enabledMercadoPagoPaymentGateway: boolean;
    isNavigateToNewOrderDetailPage: boolean;
    isDirectShipOrder: boolean;
    vendorNumbers: string[];
  }
  
  export interface NgSelect {
    id: string;
    name: string;
  }
  