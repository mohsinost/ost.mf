import { Component,OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfigService } from './services/config.service'
import {LoadingAnimationService} from './services/loading-animation.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  baseUrl: string;
  showLoading: Observable<boolean>;
  constructor(private config: ConfigService, private loadingAnimationService: LoadingAnimationService) {

  }
  ngOnInit() {
    this.baseUrl = this.config.baseUrl;
    this.showLoading = this.loadingAnimationService.busyStateObservable;
  }
}
