import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgSelect, OrderSearchRequest } from '../../../models'
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
@Component({
  selector: 'app-search-criteria',
  templateUrl: './search-criteria.component.html',
  styleUrls: ['./search-criteria.component.css']
})
export class SearchCriteriaComponent implements OnInit {

  @Output()
  search: EventEmitter<OrderSearchRequest> = new EventEmitter<OrderSearchRequest>();

  searchOptionId = 'resellerPO';
  searchValue = '';
  showSearchBox = true;
  showOrderDate = false;
  showOrderDateCalendar = false;
  showInvoiceDate = false;
  showInvoiceDateCalendar = false;
  showShipDate = false;
  showShipDateCalendar = false;
  showOrderStatus = false;

  orderDateId = 'last30days';
  invoiceDateId = 'last30days';
  shipDateId = 'last30days';
  orderStatusId = '0';

  orderDateFrom: Date;
  orderDateTo: Date;
  invoiceDateFrom: Date;
  invoiceDateTo: Date;
  shipDateFrom: Date;
  shipDateTo: Date;

  searchOptions: NgSelect[] = [
    { id: 'resellerPO', name: 'Reseller P.O. #' },
    { id: 'imOrder', name: 'IM Order #' },
    { id: 'invoice', name: 'Invoice #' },
    { id: 'endUserPO', name: 'End User P.O. #' },
    { id: 'orderDate', name: 'Order Date' },
    { id: 'invoiceDate', name: 'Invoice Date' },
    { id: 'shipDate', name: 'Ship Date' },
    { id: 'orderStatus', name: 'Order Status' },
    { id: 'sku', name: 'SKU' },
    { id: 'vpn', name: 'VPN' },
    { id: 'serialNumber', name: 'Serial Number' },
    { id: 'vendor', name: 'Vendor' },
    { id: 'bidNumber', name: 'Bid Number' },
  ];

  dateOptions: NgSelect[] = [
    { id: 'last30days', name: 'Last 30 Days' },
    { id: 'last3months', name: 'Last 3 Months' },
    { id: 'last1year', name: 'Last 1 Year' },
    { id: 'customDate', name: 'Custom Date' }
  ];

  orderStatusOptions: NgSelect[] = [
    { id: '0', name: 'All Orders' },
    { id: '1', name: 'In Progress' },
    { id: '3', name: 'Shipped' },
    { id: '5', name: 'Billed' },
    { id: '6', name: 'Order Hold' },
    { id: '7', name: 'Customer Hold' },
    { id: '9', name: 'Backorder' },
    { id: '10', name: 'Order Review' },
    { id: '11', name: 'Credit Check' },
  ]
  datePickerConfig: Partial<BsDatepickerConfig>;
  constructor() { 
    this.datePickerConfig = Object.assign({}, {
      dateInputFormat: 'MM/DD/YYYY',
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false
    });
  }

  ngOnInit(): void {

  }
  optionChanged(e: NgSelect) {
    switch (e.id) {
      case 'resellerPO':
      case 'imOrder':
      case 'invoice':
      case 'endUserPO':
      case 'sku':
      case 'vpn':
      case 'serialNumber':
      case 'vendor':
      case 'bidNumber':
        this.showSearchBox = true;
        this.showOrderDate = false;
        this.showInvoiceDate = false;
        this.showShipDate = false;
        this.showOrderStatus = false;
        break;

      case 'orderDate':
        this.showSearchBox = false;
        this.showOrderDate = true;
        this.showInvoiceDate = false;
        this.showShipDate = false;
        this.showOrderStatus = false;
        break;

      case 'invoiceDate':
        this.showSearchBox = false;
        this.showOrderDate = false;
        this.showInvoiceDate = true;
        this.showShipDate = false;
        this.showOrderStatus = false;
        break;

      case 'shipDate':
        this.showSearchBox = false;
        this.showOrderDate = false;
        this.showInvoiceDate = false;
        this.showShipDate = true;
        this.showOrderStatus = false;
        break;

      case 'orderStatus':
        this.showSearchBox = false;
        this.showOrderDate = false;
        this.showInvoiceDate = false;
        this.showShipDate = false;
        this.showOrderStatus = true;
        break;
    }
  }

  orderDateChanged(e: NgSelect) {
    this.showOrderDateCalendar = e.id === 'customDate';
  }

  invoiceDateChanged(e: NgSelect) {
    this.showInvoiceDateCalendar = e.id === 'customDate';
  }

  shipDateChanged(e: NgSelect) {
    this.showShipDateCalendar = e.id === 'customDate';
  }
  searchClicked() {
    const searchRequest = new OrderSearchRequest();
    switch (this.searchOptionId) {
      case 'resellerPO':
        searchRequest.resellerPoNumber = this.searchValue;
        break;
      case 'imOrder':
        searchRequest.imOrderNumber = this.searchValue;
        break;
      case 'invoice':
        searchRequest.invoiceNumber = this.searchValue;
        break;
      case 'endUserPO':
        searchRequest.endUserPONumber = this.searchValue;
        break;
      case 'sku':
        searchRequest.imSKU = this.searchValue;
        break;
      case 'vpn':
        searchRequest.imVPN = this.searchValue;
        break;
      case 'serialNumber':
        searchRequest.imSerialNos = this.searchValue;
        break;
      case 'vendor':
        searchRequest.imVendor = this.searchValue;
        break;
      case 'bidNumber':
        searchRequest.imBidNo = this.searchValue;
        break;
      case 'orderStatus':
        searchRequest.orderStatus = this.orderStatusId;
        break;
      case 'orderDate':
        searchRequest.toDate = new Date();
        switch (this.orderDateId) {
          case 'last30days':
            searchRequest.fromDate = new Date(new Date().setDate(new Date().getDate() - 30));
            break;
          case 'last3months':
            searchRequest.fromDate = new Date(new Date().setDate(new Date().getDate() - 90));
            break;
          case 'last1year':
            searchRequest.fromDate = new Date(new Date().setDate(new Date().getDate() - 365));
            break;
          case 'customDate':
            searchRequest.fromDate = this.orderDateFrom;
            searchRequest.toDate = this.orderDateTo;
            break;
        }
        break;
      case 'invoiceDate':
        searchRequest.invoiceDateTo = new Date();
        switch (this.invoiceDateId) {
          case 'last30days':
            searchRequest.invoiceDateFrom = new Date(new Date().setDate(new Date().getDate() - 30));
            break;
          case 'last3months':
            searchRequest.invoiceDateFrom = new Date(new Date().setDate(new Date().getDate() - 90));
            break;
          case 'last1year':
            searchRequest.invoiceDateFrom = new Date(new Date().setDate(new Date().getDate() - 365));
            break;
          case 'customDate':
            searchRequest.invoiceDateFrom = this.invoiceDateFrom;
            searchRequest.invoiceDateTo = this.invoiceDateTo;
            break;
        }
        break;
      case 'shipDate':
        searchRequest.shipDateTo = new Date();
        switch (this.shipDateId) {
          case 'last30days':
            searchRequest.shipDateFrom = new Date(new Date().setDate(new Date().getDate() - 30));
            break;
          case 'last3months':
            searchRequest.shipDateFrom = new Date(new Date().setDate(new Date().getDate() - 90));
            break;
          case 'last1year':
            searchRequest.shipDateFrom = new Date(new Date().setDate(new Date().getDate() - 365));
            break;
          case 'customDate':
            searchRequest.shipDateFrom = this.shipDateFrom;
            searchRequest.shipDateTo = this.shipDateTo;
            break;
        }
        break;
    }
    console.log("search clicked");
    this.search.emit(searchRequest);
  }

}
