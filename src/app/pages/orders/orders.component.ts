import { Component, OnInit,ViewChild,AfterViewInit } from '@angular/core';
import { MatSort,SortDirection } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { HttpClient } from '@angular/common/http';

export interface OrderList{
   endUserPO:string;
  ingramOrderNumber:string;
  orderDate:string;
  orderType:string;
  resellerPONumber:string;
  shipDate:string;
  status:string;
  total:string;
}
interface ItemsResponse {
  orderListView: Array<OrderList>;
}

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit,AfterViewInit{
  displayedColumns:string[]=['endUserPO','ingramOrderNumber','orderDate','resellerPONumber','shipDate','status','total']
  datasour:any;
  @ViewChild(MatSort) 
  sort: MatSort;
  @ViewChild(MatPaginator) 
  paginator: MatPaginator;

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
   
    let resp=this.http.get<ItemsResponse>("http://localhost:53065/api/OrderSearch");
    resp.subscribe(
                    (data)=>{
                      
                            console.log(data);
                            console.log(data.orderListView);
                            this.datasour=new MatTableDataSource(data.orderListView);
                            this.datasour.paginator = this.paginator;
                            this.datasour.sort=this.sort;
                          });
                         
       }
  
  ngAfterViewInit() {
   
}
  
}
 